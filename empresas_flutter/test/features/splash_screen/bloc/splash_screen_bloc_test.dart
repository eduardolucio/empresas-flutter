import 'package:bloc_test/bloc_test.dart';
import 'package:empresas_flutter/core/adapters/either_adapter.dart';
import 'package:empresas_flutter/core/bloc/app_bloc.dart';
import 'package:empresas_flutter/core/errors/errors.dart';
import 'package:empresas_flutter/features/login/domain/entities/logged_user.dart';
import 'package:empresas_flutter/features/login/domain/usecases/get_logged_user.dart';
import 'package:empresas_flutter/features/splash_screen/bloc/splash_screen_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class GetLoggedUserMock extends Mock implements GetLoggedUser {}
class AppBlocMock extends Mock implements AppBloc {}


main() {
  late SplashScreenBloc bloc;
  late GetLoggedUserMock getLoggedUserMock;
  late AppBlocMock appBlocMock;

  
  setUpAll((){
    registerFallbackValue<AppEvent>(LogInEvent(LoggedUser(accessToken: '', client: '', uid: '')));
  });
  
  setUp((){
    getLoggedUserMock = GetLoggedUserMock();
    appBlocMock = AppBlocMock();
    bloc = SplashScreenBloc(getLoggedUser: getLoggedUserMock, appBloc: appBlocMock);
  });

  final tLoggedUser = LoggedUser(accessToken: "accessToken", uid: "uid", client: "client");


  test('initial state should be empty', () {
    expect(bloc.state, equals(Empty()));
  });

  blocTest<SplashScreenBloc, SplashScreenState>(
    'should emit [Loading,Loaded(true)] and fire LogIn event when getLoggedUser is successful',
    build: () {
      when(() => getLoggedUserMock()).thenAnswer((_) async => RightAdapter(tLoggedUser));
      when(() => appBlocMock.add(any())).thenAnswer((_) => 0);
      return bloc;
    },
    act: (bloc) => bloc.add(CheckLogin()),
    expect: () => [Loading(), Loaded(true)],
    verify: (_){
      verify(() => appBlocMock.add(LogInEvent(tLoggedUser)));
    }
  );

  blocTest<SplashScreenBloc, SplashScreenState>(
    'should emit [Loading,Loaded(false)] when getLoggedUser fails',
    build: () {
      when(() => getLoggedUserMock()).thenAnswer((_) async => LeftAdapter(LoginExpiredFailure()));
      return bloc;
    },
    act: (bloc) => bloc.add(CheckLogin()),
    expect: () => [Loading(), Loaded(false)],
  );
}