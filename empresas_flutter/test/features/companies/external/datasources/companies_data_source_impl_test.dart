import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:empresas_flutter/core/constants.dart';
import 'package:empresas_flutter/core/errors/exceptions.dart';
import 'package:empresas_flutter/core/utils/custom_dio.dart';
import 'package:empresas_flutter/features/companies/external/datasources/companies_data_source_impl.dart';
import 'package:empresas_flutter/features/companies/infra/models/company_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../../fixtures/fixture_reader.dart';

class DioMock extends Mock implements Dio {}
class CustomDioMock extends Mock implements CustomDio {}

main() {
  late CustomDioMock customDioMock;
  late DioMock dioMock;
  late CompaniesDataSourceImpl dataSource;

  setUp(() {
    dioMock = DioMock();
    customDioMock = CustomDioMock();
    dataSource = CompaniesDataSourceImpl(customDio: customDioMock);

    when(() => customDioMock.client).thenReturn(dioMock);
  });

  void setUpMockHttpClientSuccess200() {
    when(
      () => dioMock.get(any()),
    ).thenAnswer(
      (_) async => Response(
        data: json.decode(fixture('companies/companies.json')),
        requestOptions: RequestOptions(path: ""),
        statusCode: 200,
      ),
    );
  }

  void setUpMockHttpClientNotFound404() {
    when(
      () => dioMock.get(any()),
    ).thenAnswer(
      (_) async => Response(
          data: "", requestOptions: RequestOptions(path: ""), statusCode: 404),
    );
  }

  group('getCompaniesByName', () {
    final tSearchName = 'company';

    final tCompanyModelList = (json
            .decode(fixture('companies/companies.json'))['enterprises'] as List)
        .map((e) => CompanyModel.fromJson(e));

    test('should perform a GET request with the proper searchName', () async {
      setUpMockHttpClientSuccess200();

      final result = await dataSource.getCompaniesByName(tSearchName);

      final url = "$baseApiUrl/enterprises?name=$tSearchName";
      expect(result, equals(tCompanyModelList));
      verify(() => dioMock.get(url));
    });

    test('should trhow ServerException when request fails', () async {
      setUpMockHttpClientNotFound404();

      final call = dataSource.getCompaniesByName;

      expect(() => call(tSearchName), throwsA(TypeMatcher<ServerException>()));
    });
  });
}
