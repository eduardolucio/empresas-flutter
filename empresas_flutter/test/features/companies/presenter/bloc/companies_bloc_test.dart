

import 'package:bloc_test/bloc_test.dart';
import 'package:empresas_flutter/core/adapters/either_adapter.dart';
import 'package:empresas_flutter/core/errors/errors.dart';
import 'package:empresas_flutter/features/companies/domain/entities/company.dart';
import 'package:empresas_flutter/features/companies/domain/usecases/get_companies_by_name.dart';
import 'package:empresas_flutter/features/companies/presenter/bloc/companies_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class GetCompaniesByNameMock extends Mock implements GetCompaniesByName{}
main() {
  late CompaniesBloc bloc;
  late GetCompaniesByNameMock getCompaniesByNameMock;


  setUp(() {
    getCompaniesByNameMock = GetCompaniesByNameMock();
    bloc = CompaniesBloc(getCompaniesByName: getCompaniesByNameMock);
  });

  final tCompaniesList = [Company(name: 'company', description: '')];
  final tSearchNAme = 'company';


  test('initial state should be Empty', () {
    expect(bloc.state, equals(Empty()));
  });

  blocTest<CompaniesBloc, CompaniesState>(
    'should emit [Loading, Loaded] when getCompaniesByName is successful',
    build: () {
      when(() => getCompaniesByNameMock(any())).thenAnswer((_) async => RightAdapter(tCompaniesList));
      return bloc;
    },
    wait: Duration(milliseconds: 550),
    act: (bloc) => bloc.add(GetCompaniesByNameEvent(searchName: tSearchNAme)),
    expect: () => [Loading(), Loaded(companies: tCompaniesList)],
  );

   blocTest<CompaniesBloc, CompaniesState>(
    'should emit [Loading, Error] when getCompaniesByName fails',
    build: () {
      when(() => getCompaniesByNameMock(any())).thenAnswer((_) async => LeftAdapter(ServerFailure()));
      return bloc;
    },
    wait: Duration(milliseconds: 550),
    act: (bloc) => bloc.add(GetCompaniesByNameEvent(searchName: tSearchNAme)),
    expect: () => [Loading(), Error()],
  );

}
