

import 'package:empresas_flutter/core/adapters/either_adapter.dart';
import 'package:empresas_flutter/core/errors/errors.dart';
import 'package:empresas_flutter/features/companies/domain/entities/company.dart';
import 'package:empresas_flutter/features/companies/domain/repositories/companies_repository.dart';
import 'package:empresas_flutter/features/companies/domain/usecases/get_companies_by_name.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class CompaniesRepositoryMock extends Mock implements CompaniesRepository {}

main() {
  late GetCompaniesByName usecase;
  late CompaniesRepositoryMock repository;

  setUp((){
    repository = CompaniesRepositoryMock();
    usecase = GetCompaniesByNameImpl(repository);
  }); 

  final tCompanies = [Company(name: 'company', description: '')];
  final tSearchName = 'company';

  test('should call getCompanisByName method on CompaniesRepository', () async{
    when(() => repository.getCompaniesByName(any())).thenAnswer((_) async => RightAdapter(tCompanies));

    final result = await usecase(tSearchName);

    expect(result, equals(RightAdapter<Failure, List<Company>>(tCompanies)));
    verify(() => repository.getCompaniesByName(tSearchName));
    verifyNoMoreInteractions(repository);
  });
}