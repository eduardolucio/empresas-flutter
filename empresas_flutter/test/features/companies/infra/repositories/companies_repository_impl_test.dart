

import 'package:empresas_flutter/core/adapters/either_adapter.dart';
import 'package:empresas_flutter/core/errors/errors.dart';
import 'package:empresas_flutter/core/errors/exceptions.dart';
import 'package:empresas_flutter/features/companies/domain/entities/company.dart';
import 'package:empresas_flutter/features/companies/infra/datasources/companies_data_source.dart';
import 'package:empresas_flutter/features/companies/infra/models/company_model.dart';
import 'package:empresas_flutter/features/companies/infra/repositories/companies_repository_impl.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class CompaniesDataSourceMock extends Mock implements CompaniesDataSource {}

main() {
  late CompaniesDataSource companiesDataSource;
  late CompaniesRepositoryImpl repository;

  setUp(() {
    companiesDataSource = CompaniesDataSourceMock();
    repository = CompaniesRepositoryImpl(companiesDataSource: companiesDataSource);
  });

  final tListCompanyModel= [CompanyModel(name: 'company', description: '')];

  final tSearchName = 'company';

  group('getCompaniesByName', () {
    test('should call getCompaniesByName and return a CompanyModel', () async {
      when(() => companiesDataSource.getCompaniesByName(any()))
          .thenAnswer((_) async => tListCompanyModel);

      final result = await repository.getCompaniesByName(tSearchName);

      expect(result, RightAdapter<Failure, List<Company>>(tListCompanyModel));
      verify(() => companiesDataSource.getCompaniesByName(tSearchName));
    });

    test('should return ServerFailure when request fails', () async {
      when(() => companiesDataSource.getCompaniesByName(any()))
          .thenThrow(ServerException());

      final result = await repository.getCompaniesByName(tSearchName);

      expect(result, LeftAdapter<Failure, List<Company>>(ServerFailure()));
    });
  });
}
