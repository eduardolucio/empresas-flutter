import 'package:empresas_flutter/core/adapters/either_adapter.dart';
import 'package:empresas_flutter/core/errors/errors.dart';
import 'package:empresas_flutter/core/errors/exceptions.dart';
import 'package:empresas_flutter/features/login/domain/entities/logged_user.dart';
import 'package:empresas_flutter/features/login/domain/entities/login_credentials.dart';
import 'package:empresas_flutter/features/login/infra/datasources/login_caching_data_source.dart';
import 'package:empresas_flutter/features/login/infra/datasources/login_data_source.dart';
import 'package:empresas_flutter/features/login/infra/models/logged_user_model.dart';
import 'package:empresas_flutter/features/login/infra/repositories/login_repository_impl.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class LoginDataSourceMock extends Mock implements LoginDataSource {}

class LoginCachingDataSourceMock extends Mock
    implements LoginCachingDataSource {}

main() {
  late LoginDataSource loginDataSource;
  late LoginCachingDataSource cachingDataSource;
  late LoginRepositoryImpl repository;

  setUpAll(() {
    registerFallbackValue(LoginCredentials(email: "", password: ""));
    registerFallbackValue(LoggedUserModel(
      accessToken: '',
      client: '',
      uid: '',
      expiry: DateTime.now(),
    ));
  });

  setUp(() {
    loginDataSource = LoginDataSourceMock();
    cachingDataSource = LoginCachingDataSourceMock();
    repository = LoginRepositoryImpl(loginDataSource, cachingDataSource);

    when(() => cachingDataSource.cacheLoggedUser(any()))
        .thenAnswer((_) async => Future.value());
  });

  final tLoggedUserModel = LoggedUserModel(
      accessToken: 'x',
      client: 'y',
      uid: '1',
      expiry: DateTime.now().add(Duration(days: 1)));

  final tLoginCredentials =
      LoginCredentials(email: "a@gmail.com", password: "123");

  group('login', () {
    test(
        'should call LoginDataSource and return a LoggedUser when login is successful',
        () async {
      when(() => loginDataSource.login(any()))
          .thenAnswer((_) async => tLoggedUserModel);

      final result = await repository.login(tLoginCredentials);

      expect(
          result, equals(RightAdapter<Failure, LoggedUser>(tLoggedUserModel)));
      verify(() => loginDataSource.login(tLoginCredentials));
    });

    test('should call cacheLoggedUser when login is successful', () async {
      when(() => loginDataSource.login(any()))
          .thenAnswer((_) async => tLoggedUserModel);

      await repository.login(tLoginCredentials);

      verify(() => cachingDataSource.cacheLoggedUser(tLoggedUserModel));
    });

    test('should return LoginInvalidFailure when login throws InvalidLogin',
        () async {
      when(() => loginDataSource.login(any()))
          .thenThrow(InvalidLoginException());

      final result = await repository.login(tLoginCredentials);

      expect(result,
          equals(LeftAdapter<Failure, LoggedUser>(LoginInvalidFailure())));
    });

    test('should return LoginFailure when login throws another exception',
        () async {
      when(() => loginDataSource.login(any())).thenThrow(Exception());

      final result = await repository.login(tLoginCredentials);

      expect(result, equals(LeftAdapter<Failure, LoggedUser>(LoginFailure())));
    });
  });

  group('getLoggedUser', () {
    test('should call getUserCached and return a LoggedUser', () async {
      when(() => cachingDataSource.getUserCached())
          .thenAnswer((_) async => tLoggedUserModel);

      final result = await repository.getLoggedUser();

      expect(result, RightAdapter<Failure, LoggedUser>(tLoggedUserModel));
      verify(() => cachingDataSource.getUserCached());
    });

    test('should return LoginExpiredFailure when expiry is a past date',
        () async {
      final tExpiredUser = LoggedUserModel(
          accessToken: 'x',
          client: 'y',
          uid: '1',
          expiry: DateTime.now().subtract(Duration(days: 1)));

      when(() => cachingDataSource.getUserCached()).thenAnswer((_) async => tExpiredUser);

      final result = await repository.getLoggedUser();

      expect(result, LeftAdapter<Failure, LoggedUser>(LoginExpiredFailure()));
      verify(() => cachingDataSource.getUserCached());
    });

    test(
        'should return LoginCacheFailure when getUserCached throws another Exception',
        () async {
      when(() => cachingDataSource.getUserCached())
          .thenThrow(NoCachedDataException());

      final result = await repository.getLoggedUser();

      expect(result, LeftAdapter<Failure, LoggedUser>(LoginCacheFailure()));
      verify(() => cachingDataSource.getUserCached());
    });
  });
}
