
import 'dart:convert';

import 'package:empresas_flutter/core/errors/exceptions.dart';
import 'package:empresas_flutter/features/login/external/datasources/login_caching_data_source_impl.dart';
import 'package:empresas_flutter/features/login/infra/models/logged_user_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../fixtures/fixture_reader.dart';

class SharedPreferencesMock extends Mock implements SharedPreferences{}
main() {
  late SharedPreferencesMock sharedPreferencesMock;
  late LoginCachingDataSourceImpl dataSource;

  setUp((){
    sharedPreferencesMock = SharedPreferencesMock();
    dataSource = LoginCachingDataSourceImpl(sharedPreferencesMock);
  });

  group('getUserCached', (){
    final tUserCached = fixture('login/logged_user_model.json');
    final tLoggedUserModel = LoggedUserModel.fromJson(json.decode(tUserCached));
    test('should return LoggerUserModel from SharedPreferences when there is one in cache', () async{
      when(() => sharedPreferencesMock.getString(any())).thenReturn(tUserCached);

      final result = await dataSource.getUserCached();

      expect(result, equals(tLoggedUserModel));
      verify(() => sharedPreferencesMock.getString(cacheLoggedUserString));
    });

    test('should throw NoCachedDataException when there is not a cached value', () async{
      when(() => sharedPreferencesMock.getString(any())).thenReturn(null);

      final call = dataSource.getUserCached;

      expect(() => call(), throwsA(TypeMatcher<NoCachedDataException>()));
    });
  });

  group('cacheLoggedUser', (){
    final tUserModel = LoggedUserModel(expiry: DateTime.now(), accessToken: '123', client: '2', uid: '1');
    test(
      "should call SharedPreferences to cache the data",
      () {
        when(() => sharedPreferencesMock.setString(any(), any()))
            .thenAnswer((_) => (Future.value(true)));

        dataSource.cacheLoggedUser(tUserModel);

        final expectedJsonString = json.encode(tUserModel.toJson());

        verify(() => sharedPreferencesMock.setString(
              cacheLoggedUserString,
              expectedJsonString,
            ));
      },
    );
    
  });
}