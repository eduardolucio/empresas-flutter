import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:empresas_flutter/core/constants.dart';
import 'package:empresas_flutter/core/errors/exceptions.dart';
import 'package:empresas_flutter/core/utils/custom_dio.dart';
import 'package:empresas_flutter/features/login/domain/entities/login_credentials.dart';
import 'package:empresas_flutter/features/login/external/datasources/login_data_source_impl.dart';
import 'package:empresas_flutter/features/login/infra/models/logged_user_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../../fixtures/fixture_reader.dart';

class DioMock extends Mock implements Dio {}
class CustomDioMock extends Mock implements CustomDio {}


main() {
  late DioMock dioMock;
  late CustomDioMock customDioMock;
  late LoginDataSourceImpl dataSource;

  setUp(() {
    dioMock = DioMock();
    customDioMock = CustomDioMock();
    dataSource = LoginDataSourceImpl(customDio: customDioMock);

    when(() => customDioMock.client).thenReturn(dioMock);
  });

  void setUpMockHttpClientSuccess200() {
    when(() => dioMock.post(
          any(),
          data: any(named: 'data'),
        )).thenAnswer(
      (_) async => Response(
          data: json.decode(fixture('login/login_data.json')),
          requestOptions: RequestOptions(path: ""),
          statusCode: 200,
          headers: Headers.fromMap({
            'access-token': ["123"],
            'uid': ["abc@mail.com"],
            'expiry': ["1628165437"],
            'client': ["client"]
          })),
    );
  }

  void setUpMockHttpClient200WithoutUid() {
    when(() => dioMock.post(
          any(),
          data: any(named: 'data'),
        )).thenAnswer(
      (_) async => Response(
          data: json.decode(fixture('login/login_data.json')),
          requestOptions: RequestOptions(path: ""),
          statusCode: 200,
          headers: Headers.fromMap({
            'access-token': ["123"],
            'expiry': ["1628165437"],
            'client': ["client"]
          })),
    );
  }

  void setUpMockHttpClientUnauthorized401() {
    when(() => dioMock.post(
          any(),
          data: any(named: 'data'),
        )).thenAnswer(
      (_) async => Response(
          data: "Invalid login credentials. Please try again.",
          requestOptions: RequestOptions(path: ""),
          statusCode: 401),
    );
  }

  void setUpMockHttpClientUnauthorized404() {
    when(() => dioMock.post(
          any(),
          data: any(named: 'data'),
        )).thenAnswer(
      (_) async => Response(
          data: "",
          requestOptions: RequestOptions(path: ""),
          statusCode: 404),
    );
  }

  group('login', () {
    final tLoginCredentials =
        LoginCredentials(email: "abc@mail.com", password: "123456");

    final tUserModel = LoggedUserModel(
        expiry: DateTime.fromMillisecondsSinceEpoch(1628165437000),
        accessToken: "123",
        client: "client",
        uid: "abc@mail.com");

    test('should perform a POST request with the proper email and password',
        () {
      setUpMockHttpClientSuccess200();

      dataSource.login(tLoginCredentials);

      final url = "$baseApiUrl/users/auth/sign_in";
      verify(() => dioMock.post(url, data: {
            'email': tLoginCredentials.email,
            'password': tLoginCredentials.password
          }));
    });

    test('should return a LoggedUserModel when request(200) sucessful',
        () async {
      setUpMockHttpClientSuccess200();

      final result = await dataSource.login(tLoginCredentials);

      expect(result, equals(tUserModel));
    });

    test('should throw InvalidLogin when request(401) unauthorized',
        () async {
      setUpMockHttpClientUnauthorized401();

      final call = dataSource.login;

      expect(() => call(tLoginCredentials), throwsA(TypeMatcher<InvalidLoginException>()));
    });

    test('should throw LoginError when request fail',
        () async {
      setUpMockHttpClientUnauthorized404();

      final call = dataSource.login;

      expect(() => call(tLoginCredentials), throwsA(TypeMatcher<LoginErrorException>()));
    });

    test('should throw LoginError when one of the headers is null',
        () async {
      setUpMockHttpClient200WithoutUid();

      final call = dataSource.login;

      expect(() => call(tLoginCredentials), throwsA(TypeMatcher<LoginErrorException>()));
    });
  });
}
