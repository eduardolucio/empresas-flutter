import 'package:bloc_test/bloc_test.dart';
import 'package:empresas_flutter/core/adapters/either_adapter.dart';
import 'package:empresas_flutter/core/bloc/app_bloc.dart';
import 'package:empresas_flutter/core/errors/errors.dart';
import 'package:empresas_flutter/features/login/domain/entities/logged_user.dart';
import 'package:empresas_flutter/features/login/domain/entities/login_credentials.dart';
import 'package:empresas_flutter/features/login/domain/usecases/login.dart';
import 'package:empresas_flutter/features/login/presenter/bloc/login_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class LoginMock extends Mock implements Login {}

class AppBlocMock extends Mock implements AppBloc {}

main() {
  late LoginBloc bloc;
  late Login loginMock;
  late AppBloc appBlocMock;

  final tLoggedUser = LoggedUser(accessToken: "accessToken", uid: "uid", client: "client");
  final tLoginCredentials =  LoginCredentials(email: 'a@mail.com', password: '123');

  setUpAll((){
    registerFallbackValue(LoginCredentials(email: '', password: ''));
    registerFallbackValue<AppEvent>(LogInEvent(tLoggedUser));
  });

  setUp(() {
    loginMock = LoginMock();
    appBlocMock = AppBlocMock();
    bloc = LoginBloc(appBloc: appBlocMock, login: loginMock);
  });

  


  test('initial state should be empty', () {
    expect(bloc.state, equals(Empty()));
  });

  blocTest<LoginBloc, LoginState>(
    'should emit [Loading,LoginSuccess] and fire LogIn event when login is successfull',
    build: () {
      when(() => loginMock(any())).thenAnswer((_) async => RightAdapter(tLoggedUser));
      when(() => appBlocMock.add(any())).thenAnswer((_) => 0);
      return bloc;
    },
    act: (bloc) => bloc.add(TryLogin(credentials: tLoginCredentials)),
    expect: () => [Loading(), LoginSuccess()],
    verify: (_){
      verify(() => appBlocMock.add(LogInEvent(tLoggedUser)));
    }
  );

  blocTest<LoginBloc, LoginState>(
    'should emit [Loading,LoginFailed] when login returns a LoginInvalidFailure',
    build: () {
      when(() => loginMock(any())).thenAnswer((_) async => LeftAdapter(LoginInvalidFailure()));
      return bloc;
    },
    act: (bloc) => bloc.add(TryLogin(credentials: tLoginCredentials)),
    expect: () => [Loading(), LoginFailed(message: loginInvalidMessage)],
  );

  blocTest<LoginBloc, LoginState>(
    'should emit [Loading,LoginFailed] when login returns a LoginFailure',
    build: () {
      when(() => loginMock(any())).thenAnswer((_) async => LeftAdapter(LoginFailure()));
      return bloc;
    },
    act: (bloc) => bloc.add(TryLogin(credentials: tLoginCredentials)),
    expect: () => [Loading(), LoginFailed(message: loginFailureMessage)],
  );

}
