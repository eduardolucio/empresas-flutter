import 'package:empresas_flutter/core/adapters/either_adapter.dart';
import 'package:empresas_flutter/core/errors/errors.dart';
import 'package:empresas_flutter/features/login/domain/entities/logged_user.dart';
import 'package:empresas_flutter/features/login/domain/entities/login_credentials.dart';
import 'package:empresas_flutter/features/login/domain/repositories/login_repository.dart';
import 'package:empresas_flutter/features/login/domain/usecases/login.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class LoginRepositoryMock extends Mock implements LoginRepository {}

main() {
  late Login usecase;
  late LoginRepository repository;

  setUp((){
    repository = LoginRepositoryMock();
    usecase = LoginImpl(repository);

    registerFallbackValue(LoginCredentials(email: "email@x.com", password: "123"));
  });

  final tLoggedUser = LoggedUser(accessToken: "accessToken", uid: "uid", client: "client");
  final tUserCredentials = LoginCredentials(email: "email@gmail.com", password: "123456");

  test('should call login method on LoginRepository', () async{
    when(() => repository.login(any())).thenAnswer((_) async => RightAdapter(tLoggedUser));

    final result = await usecase(tUserCredentials);

    expect(result, equals(RightAdapter<Failure, LoggedUser>(tLoggedUser)));
    verify(() => repository.login(tUserCredentials));
    verifyNoMoreInteractions(repository);
  });

  test('should return LoginEmailFailure when the email informed isnt a valid email', () async {
    final result = await usecase(LoginCredentials(email: "", password: ""));

    expect(result, equals(LeftAdapter<Failure, LoggedUser>(LoginEmailFailure())));
  });
}