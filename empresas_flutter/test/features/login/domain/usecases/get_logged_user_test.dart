
import 'package:empresas_flutter/core/adapters/either_adapter.dart';
import 'package:empresas_flutter/core/errors/errors.dart';
import 'package:empresas_flutter/features/login/domain/entities/logged_user.dart';
import 'package:empresas_flutter/features/login/domain/entities/login_credentials.dart';
import 'package:empresas_flutter/features/login/domain/repositories/login_repository.dart';
import 'package:empresas_flutter/features/login/domain/usecases/get_logged_user.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class LoginRepositoryMock extends Mock implements LoginRepository {}

main() {
  late GetLoggedUser usecase;
  late LoginRepository repository;

  setUp((){
    repository = LoginRepositoryMock();
    usecase = GetLoggedUserImpl(repository);

    registerFallbackValue(LoginCredentials(email: "email@x.com", password: "123"));
  }); 

  final tLoggedUser = LoggedUser(accessToken: "accessToken", uid: "uid", client: "client");

  test('should call getLoggedUser method on LoginRepository', () async{
    when(() => repository.getLoggedUser()).thenAnswer((_) async => RightAdapter(tLoggedUser));

    final result = await usecase();

    expect(result, equals(RightAdapter<Failure, LoggedUser>(tLoggedUser)));
    verify(() => repository.getLoggedUser());
    verifyNoMoreInteractions(repository);
  });
}