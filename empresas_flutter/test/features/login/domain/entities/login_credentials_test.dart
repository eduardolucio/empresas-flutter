import 'package:empresas_flutter/features/login/domain/entities/login_credentials.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  group('checkEmail', (){
    test('should return false when email is empty', () {
      expect(LoginCredentials(email: "", password: "").checkEmail, equals(false));
    });

    test('should return false when email is invalid', () {
      expect(LoginCredentials(email: "abc12", password: "").checkEmail, equals(false));
    });

    test('should return true when email is valid', () {
      expect(LoginCredentials(email: "eduardo@gmail.com", password: "").checkEmail, equals(true));
    });
  });
}