import 'package:bloc_test/bloc_test.dart';
import 'package:empresas_flutter/core/bloc/app_bloc.dart';
import 'package:empresas_flutter/features/login/domain/entities/logged_user.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  late AppBloc bloc;

  setUp(() {
    bloc = AppBloc();
  });

  final tLoggedUser = LoggedUser(accessToken: "accessToken", uid: "uid", client: "client");


  test('initial state should be Unauthenticated', () {
    expect(bloc.state, equals(Unauthenticated()));
  });

  blocTest<AppBloc, AppState>(
    'should emit [Authenticated] when receive LogInEvent',
    build: () => bloc,
    act: (bloc) => bloc.add(LogInEvent(tLoggedUser)),
    expect: () => [Authenticated(tLoggedUser)],
  );

  blocTest<AppBloc, AppState>(
    'should emit [Unauthenticated] when receive ExpiredLoginEvent',
    build: () => bloc,
    act: (bloc) => bloc.add(ExpiredLoginEvent()),
    expect: () => [Unauthenticated()],
  );

}
