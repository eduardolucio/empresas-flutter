import 'package:empresas_flutter/features/splash_screen/bloc/splash_screen_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';

class SplashScreenPage extends StatefulWidget {
  const SplashScreenPage({Key? key}) : super(key: key);

  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  final bloc = Modular.get<SplashScreenBloc>();

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration(seconds: 1))
        .then((value) => bloc.add(CheckLogin()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //appBar: AppBar(),
      body: buildBody(context),
    );
  }

  Widget buildBody(context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.bottomLeft,
              end: Alignment.topRight,
              colors: [
            Color(0xffe08db8),
            Color(0xffc55d94),
            Color(0xffb2217a),
            Color(0xffb42391),
            Color(0xffae269b),
            Color(0xff9831b2),
            Color(0xff7c39bf),
          ])),
      child: BlocBuilder(
          bloc: bloc,
          builder: (context, state) {
            if (state is Empty || state is Loading) {
              return Center(
                child: Image.asset('assets/logo_home.png'),
              );
            } else if (state is Loaded) {
              if (state.logged) {
                Modular.to.pushReplacementNamed('/companies');
              } else {
                Modular.to.pushReplacementNamed('/login');
              }
            }
            return Container();
          }),
    );
  }
}
