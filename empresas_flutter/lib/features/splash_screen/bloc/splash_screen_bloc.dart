import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:empresas_flutter/core/bloc/app_bloc.dart';
import 'package:empresas_flutter/features/login/domain/usecases/get_logged_user.dart';
import 'package:equatable/equatable.dart';

part 'splash_screen_event.dart';
part 'splash_screen_state.dart';

class SplashScreenBloc extends Bloc<SplashScreenEvent, SplashScreenState> {
  SplashScreenBloc({required this.getLoggedUser, required this.appBloc})
      : super(Empty());

  final GetLoggedUser getLoggedUser;
  final AppBloc appBloc;

  @override
  Stream<SplashScreenState> mapEventToState(
    SplashScreenEvent event,
  ) async* {
    if (event is CheckLogin) {
      yield Loading();
      final either = await getLoggedUser();

      yield* either.fold(
        (l) async*{
          yield Loaded(false);
        },
        (loggedUser) async* {
          appBloc.add(LogInEvent(loggedUser));
          yield Loaded(true);
        },
      );
    }
  }
}
