part of 'splash_screen_bloc.dart';

abstract class SplashScreenState extends Equatable{}

class Empty extends SplashScreenState {
  @override
  List<Object?> get props => [];
}

class Loading extends SplashScreenState {
  @override
  List<Object?> get props => [];
}

class Loaded extends SplashScreenState {
  final bool logged;

  Loaded(this.logged);

  @override
  List<Object?> get props => [logged];
}
