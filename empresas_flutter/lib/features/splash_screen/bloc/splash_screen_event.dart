part of 'splash_screen_bloc.dart';

abstract class SplashScreenEvent extends Equatable {}

class CheckLogin extends SplashScreenEvent {
  @override
  List<Object?> get props => [];
}