import 'package:empresas_flutter/core/constants.dart';
import 'package:empresas_flutter/core/errors/exceptions.dart';
import 'package:empresas_flutter/core/utils/custom_dio.dart';
import 'package:empresas_flutter/features/companies/domain/entities/company.dart';
import 'package:empresas_flutter/features/companies/infra/datasources/companies_data_source.dart';
import 'package:empresas_flutter/features/companies/infra/models/company_model.dart';

class CompaniesDataSourceImpl extends CompaniesDataSource{
  final CustomDio customDio;

  CompaniesDataSourceImpl({required this.customDio});

  @override
  Future<List<Company>> getCompaniesByName(String searchName) async{
    final response = await customDio.client.get("$baseApiUrl/enterprises?name=$searchName");

    if(response.statusCode == 200){
      return (response.data['enterprises'] as List).map((company) => CompanyModel.fromJson(company)).toList();
    }else{
      throw ServerException();
    }

  }

}