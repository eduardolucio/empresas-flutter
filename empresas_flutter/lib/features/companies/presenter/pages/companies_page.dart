import 'package:empresas_flutter/core/utils/custom_circular_progress_indicator.dart';
import 'package:empresas_flutter/core/utils/text_field_decoration.dart';
import 'package:empresas_flutter/features/companies/domain/entities/company.dart';
import 'package:empresas_flutter/features/companies/presenter/bloc/companies_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:google_fonts/google_fonts.dart';

class CompaniesPage extends StatefulWidget {
  const CompaniesPage({Key? key}) : super(key: key);

  @override
  _CompaniesPageState createState() => _CompaniesPageState();
}

class _CompaniesPageState extends State<CompaniesPage> {
  FocusNode _focus = FocusNode();
  final bloc = Modular.get<CompaniesBloc>();

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Stack(
        children: [
          topBar(screenWidth, _focus.hasFocus),
          Column(
            children: [
              SizedBox(
                height: _focus.hasFocus ? 45 : 125,
              ),
              searchBar(),
              Expanded(
                child: BlocBuilder<CompaniesBloc, CompaniesState>(
                  bloc: bloc,
                  builder: (context, state) {
                    if (state is Loading) {
                      return Center(
                        child: CustomCircularProgressIndicator(),
                      );
                    } else if (state is Loaded) {
                      final companies = state.companies;

                      if (companies.length == 0) {
                        return nothingFoundMessage(context);
                      } else {
                        return companiesListBuilder(companies);
                      }
                    } else if (state is Error) {
                      return errorMessage();
                    }
                    return Container();
                  },
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  Center errorMessage() {
    return Center(
      child: Text('Houve algum erro, tente novamente mais tarde!'),
    );
  }

  Center nothingFoundMessage(BuildContext context) {
    return Center(
      child: Text(
        'Nenhum resultado encontrado',
        style: GoogleFonts.rubik(
            fontSize: 18, color: Theme.of(context).primaryColorDark),
      ),
    );
  }

  Widget companiesListBuilder(List<Company> companies) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '${companies.length} resultados encontrados',
            style: GoogleFonts.rubik(
                fontSize: 14,
                color: Theme.of(context).primaryColorDark,
                fontWeight: FontWeight.w300),
          ),
          Expanded(
            child: ListView.builder(
              itemCount: companies.length,
              itemBuilder: (context, index) {
                return companyButton(index, companies[index]);
              },
            ),
          ),
        ],
      ),
    );
  }

  Padding companyButton(int index, Company company) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10.0),
      child: SizedBox(
        height: 120,
        child: TextButton(
          style: TextButton.styleFrom(
            backgroundColor:
                (index % 2 == 0) ? Color(0xff79BBCA) : Color(0xffEB9797),
          ),
          onPressed: () =>
              Modular.to.pushNamed('/companies/detail', arguments: company),
          child: Center(
            child: Text(
              company.name.toUpperCase(),
              style: GoogleFonts.rubik(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }

  Widget searchBar() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: TextField(
        onChanged: (value) =>
            bloc.add(GetCompaniesByNameEvent(searchName: value)),
        focusNode: _focus,
        style: GoogleFonts.rubik(
            fontWeight: FontWeight.w300,
            fontSize: 18,
            color: Theme.of(context).primaryColorDark),
        decoration: txtFieldDecoration(
          preffixIcon: Icon(
            Icons.search,
            size: 30,
            color: Theme.of(context).primaryColorDark,
          ),
          hintText: 'Pesquise por empresa',
        ),
      ),
    );
  }

  Container topBar(double screenWidth, bool collapsed) {
    return Container(
      height: collapsed ? 75 : 160,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.bottomLeft,
          end: Alignment.topRight,
          colors: [
            Color(0xffb2217a),
            Color(0xffb42391),
            Color(0xff9831b2),
          ],
        ),
      ),
      child: collapsed
          ? Container()
          : Stack(
              children: [
                logoPositioned(
                  left: screenWidth / 5,
                  angle: -2.3,
                  top: -30,
                  width: screenWidth / 2,
                ),
                logoPositioned(
                    angle: 0.47,
                    width: screenWidth / 3,
                    bottom: -20,
                    left: -25),
                logoPositioned(
                    angle: -2.85, width: screenWidth / 3, bottom: 0, right: 0)
              ],
            ),
    );
  }

  Positioned logoPositioned(
      {double? left,
      double? top,
      double? bottom,
      double? right,
      required double angle,
      required double width}) {
    return Positioned(
      left: left,
      top: top,
      right: right,
      bottom: bottom,
      child: Transform.rotate(
        angle: angle,
        child: Image.asset(
          'assets/logo_bigger.png',
          width: width,
        ),
      ),
    );
  }
}
