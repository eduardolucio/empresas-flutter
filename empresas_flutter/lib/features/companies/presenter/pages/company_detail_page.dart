import 'package:empresas_flutter/features/companies/domain/entities/company.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:google_fonts/google_fonts.dart';

class CompanyDetailPage extends StatelessWidget {
  final Company company;

  const CompanyDetailPage({Key? key, required this.company}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: buildAppBar(context),
      body: SingleChildScrollView(
        child: Column(
          children: [
            companyTitleContainer(),
            descriptionText(),
          ],
        ),
      ),
    );
  }

  Padding descriptionText() {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Text(
        company.description,
        style: GoogleFonts.rubik(fontSize: 18, fontWeight: FontWeight.w300),
      ),
    );
  }

  Container companyTitleContainer() {
    return Container(
      height: 120,
      color: Color(0xff79BBCA),
      child: Center(
        child: Text(
          company.name.toUpperCase(),
          style: GoogleFonts.rubik(
              fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white),
        ),
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      leading: Padding(
        padding: const EdgeInsets.only(left: 15.0, top: 8.0, bottom: 8.0),
        child: TextButton(
            style: TextButton.styleFrom(
              backgroundColor: Theme.of(context).primaryColorLight,
            ),
            onPressed: () => Modular.to.pop(),
            child: Icon(
              Icons.arrow_back,
              color: Theme.of(context).primaryColor,
            )),
      ),
      centerTitle: true,
      backgroundColor: Colors.white,
      elevation: 0,
      title: Text(
        company.name,
        style: GoogleFonts.rubik(
            fontSize: 20,
            color: Theme.of(context).primaryColorDark,
            fontWeight: FontWeight.w500),
      ),
    );
  }
}
