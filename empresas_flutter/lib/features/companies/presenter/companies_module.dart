import 'package:empresas_flutter/features/companies/domain/usecases/get_companies_by_name.dart';
import 'package:empresas_flutter/features/companies/external/datasources/companies_data_source_impl.dart';
import 'package:empresas_flutter/features/companies/infra/repositories/companies_repository_impl.dart';
import 'package:empresas_flutter/features/companies/presenter/bloc/companies_bloc.dart';
import 'package:empresas_flutter/features/companies/presenter/pages/companies_page.dart';
import 'package:empresas_flutter/features/companies/presenter/pages/company_detail_page.dart';
import 'package:flutter_modular/flutter_modular.dart';

class CompaniesModule extends Module {
  @override
  List<Bind<Object>> get binds => [
    Bind.lazySingleton((i) => CompaniesDataSourceImpl(customDio: i())),
    Bind.lazySingleton((i) => CompaniesRepositoryImpl(companiesDataSource: i())),
    Bind.lazySingleton((i) => GetCompaniesByNameImpl(i())),
    Bind.factory((i) => CompaniesBloc(getCompaniesByName: i()))
  ];

  @override
  List<ModularRoute> get routes => [
    ChildRoute('/', child: (_, args) => CompaniesPage()),
    ChildRoute('/detail', child: (_, args) => CompanyDetailPage(company: args.data))
  ];
}