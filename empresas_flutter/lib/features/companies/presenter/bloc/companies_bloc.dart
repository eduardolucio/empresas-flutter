import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:empresas_flutter/features/companies/domain/entities/company.dart';
import 'package:empresas_flutter/features/companies/domain/usecases/get_companies_by_name.dart';
import 'package:equatable/equatable.dart';
import 'package:rxdart/rxdart.dart' as rxdart;

part 'companies_event.dart';
part 'companies_state.dart';

class CompaniesBloc extends Bloc<CompaniesEvent, CompaniesState> {
  CompaniesBloc({required this.getCompaniesByName}) : super(Empty());

  final GetCompaniesByName getCompaniesByName;

  @override
  Stream<Transition<CompaniesEvent, CompaniesState>> transformEvents(
      Stream<CompaniesEvent> events,
      TransitionFunction<CompaniesEvent, CompaniesState> transitionFn) {
    return super.transformEvents(events.debounceTime(Duration(milliseconds: 500)), transitionFn);
  }

  @override
  Stream<CompaniesState> mapEventToState(
    CompaniesEvent event,
  ) async* {
    if (event is GetCompaniesByNameEvent) {
      yield Loading();
      final either = await getCompaniesByName(event.searchName);

      yield* either.fold(
        (l) async* {
          yield Error();
        },
        (companiesList) async* {
          yield Loaded(companies: companiesList);
        },
      );
    }
  }
}
