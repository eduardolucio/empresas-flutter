part of 'companies_bloc.dart';

abstract class CompaniesEvent extends Equatable{}

class GetCompaniesByNameEvent extends CompaniesEvent{
  final String searchName;

  GetCompaniesByNameEvent({required this.searchName});

  @override
  List<Object?> get props => [searchName];

}