part of 'companies_bloc.dart';

abstract class CompaniesState extends Equatable{}

class Empty extends CompaniesState {
  @override
  List<Object?> get props => [];
}

class Loading extends CompaniesState {
  @override
  List<Object?> get props => [];
}

class Loaded extends CompaniesState {
  final List<Company> companies;

  Loaded({required this.companies});

  @override
  List<Object?> get props => [companies];  
}

class Error extends CompaniesState{
  final String? message;

  Error({this.message});

  @override
  List<Object?> get props => [message];
}
