import 'package:equatable/equatable.dart';

class Company extends Equatable{
  final String name;
  final String description;

  Company({required this.name, required this.description});

  @override
  List<Object?> get props => [name, description]; 
}