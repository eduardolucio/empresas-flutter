import 'package:empresas_flutter/core/adapters/either_adapter.dart';
import 'package:empresas_flutter/core/errors/errors.dart';
import 'package:empresas_flutter/features/companies/domain/entities/company.dart';

abstract class CompaniesRepository {
  Future<EitherAdapter<Failure, List<Company>>> getCompaniesByName(String searchName);
}
