import 'package:empresas_flutter/core/adapters/either_adapter.dart';
import 'package:empresas_flutter/core/errors/errors.dart';
import 'package:empresas_flutter/features/companies/domain/entities/company.dart';
import 'package:empresas_flutter/features/companies/domain/repositories/companies_repository.dart';

abstract class GetCompaniesByName{
      Future<EitherAdapter<Failure, List<Company>>> call(String name);
}

class GetCompaniesByNameImpl implements GetCompaniesByName{
  final CompaniesRepository repository;

  GetCompaniesByNameImpl(this.repository);

  @override
  Future<EitherAdapter<Failure, List<Company>>> call(String name) {
    return repository.getCompaniesByName(name);
  }

}