import 'package:empresas_flutter/features/companies/domain/entities/company.dart';

abstract class CompaniesDataSource{
  Future<List<Company>> getCompaniesByName(String searchName);
}