import 'package:empresas_flutter/core/adapters/either_adapter.dart';
import 'package:empresas_flutter/core/errors/errors.dart';
import 'package:empresas_flutter/core/errors/exceptions.dart';
import 'package:empresas_flutter/features/companies/domain/entities/company.dart';
import 'package:empresas_flutter/features/companies/domain/repositories/companies_repository.dart';
import 'package:empresas_flutter/features/companies/infra/datasources/companies_data_source.dart';

class CompaniesRepositoryImpl extends CompaniesRepository {
  final CompaniesDataSource companiesDataSource;
  
  CompaniesRepositoryImpl({
    required this.companiesDataSource,
  });

  @override
  Future<EitherAdapter<Failure, List<Company>>> getCompaniesByName(
      String searchName) async{
        try {
          return RightAdapter(await companiesDataSource.getCompaniesByName(searchName));
        } on ServerException {
          return LeftAdapter(ServerFailure());
        }
  }
}
