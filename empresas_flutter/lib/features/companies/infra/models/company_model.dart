import 'package:empresas_flutter/features/companies/domain/entities/company.dart';

class CompanyModel extends Company {
  CompanyModel({required String name, required String description})
      : super(name: name, description: description);

  factory CompanyModel.fromJson(Map<String, dynamic> jsonMap) {
    return CompanyModel(
      name: jsonMap['enterprise_name'],
      description: jsonMap['description'],
    );
  }
}
