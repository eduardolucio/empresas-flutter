import 'package:equatable/equatable.dart';
import 'package:string_validator/string_validator.dart' as validator;


class LoginCredentials extends Equatable {
  final String email;
  final String password;

  LoginCredentials({required this.email, required this.password});

  bool get checkEmail => validator.isEmail(email);

  @override
  List<Object?> get props => [email, password];
}