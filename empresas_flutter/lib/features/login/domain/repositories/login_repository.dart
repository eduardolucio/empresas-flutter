

import 'package:empresas_flutter/core/adapters/either_adapter.dart';
import 'package:empresas_flutter/core/errors/errors.dart';
import 'package:empresas_flutter/features/login/domain/entities/logged_user.dart';
import 'package:empresas_flutter/features/login/domain/entities/login_credentials.dart';

abstract class LoginRepository{
  Future<EitherAdapter<Failure, LoggedUser>> login(LoginCredentials credentials);
  Future<EitherAdapter<Failure, LoggedUser>> getLoggedUser();
}