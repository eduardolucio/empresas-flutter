import 'package:empresas_flutter/core/adapters/either_adapter.dart';
import 'package:empresas_flutter/core/errors/errors.dart';
import 'package:empresas_flutter/features/login/domain/entities/logged_user.dart';
import 'package:empresas_flutter/features/login/domain/repositories/login_repository.dart';

abstract class GetLoggedUser{
    Future<EitherAdapter<Failure, LoggedUser>> call();
}

class GetLoggedUserImpl implements GetLoggedUser{
  final LoginRepository repository;

  GetLoggedUserImpl(this.repository);

  @override
  Future<EitherAdapter<Failure, LoggedUser>> call() {
    return repository.getLoggedUser();
  }

}