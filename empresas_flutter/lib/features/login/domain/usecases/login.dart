import 'package:empresas_flutter/core/adapters/either_adapter.dart';
import 'package:empresas_flutter/core/errors/errors.dart';
import 'package:empresas_flutter/features/login/domain/entities/logged_user.dart';
import 'package:empresas_flutter/features/login/domain/entities/login_credentials.dart';
import 'package:empresas_flutter/features/login/domain/repositories/login_repository.dart';

abstract class Login{
  Future<EitherAdapter<Failure, LoggedUser>> call(LoginCredentials credentials);
}

class LoginImpl implements Login{
  final LoginRepository repository;

  LoginImpl(this.repository);

  @override
  Future<EitherAdapter<Failure, LoggedUser>> call(LoginCredentials credentials) async{
    if(!credentials.checkEmail){
      return LeftAdapter(LoginEmailFailure());
    }

    return repository.login(credentials);
  }

}