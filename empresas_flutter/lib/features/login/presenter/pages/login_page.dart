import 'package:empresas_flutter/core/utils/custom_circular_progress_indicator.dart';
import 'package:empresas_flutter/core/utils/text_field_decoration.dart';
import 'package:empresas_flutter/features/login/domain/entities/login_credentials.dart';
import 'package:empresas_flutter/features/login/presenter/bloc/login_bloc.dart';
import 'package:empresas_flutter/features/login/presenter/pages/widgets/custom_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  final loginBloc = Modular.get<LoginBloc>();
  bool invalidLogin = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: BlocBuilder<LoginBloc, LoginState>(
        bloc: loginBloc,
        builder: (context, state) {
          if (state is LoginSuccess) {
            Modular.to.pushReplacementNamed('/companies');
            return Container();
          } else {
            if (state is LoginFailed) {
              invalidLogin = true;
            }

            return Stack(
              children: [
                buildScrollView(context),
                if (state is Loading) loadingOverlay(),
              ],
            );
          }
        },
      ),
    );
  }

  Positioned loadingOverlay() {
    return Positioned.fill(
      child: Container(
        color: Colors.black.withOpacity(0.3),
        child: Center(child: CustomCircularProgressIndicator()),
      ),
    );
  }

  CustomScrollView buildScrollView(BuildContext context) {
    return CustomScrollView(
      slivers: [
        SliverPersistentHeader(
          delegate: CustomAppBar(height: 257),
          floating: true,
          pinned: true,
        ),
        SliverPadding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          sliver: loginFormSliverList(context),
        )
      ],
    );
  }

  SliverList loginFormSliverList(BuildContext context) {
    return SliverList(
      delegate: SliverChildListDelegate(
        [
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 5.0, bottom: 2.0),
            child: Text(
              'Email',
              style: GoogleFonts.rubik(fontSize: 14),
            ),
          ),
          TextField(
            decoration: loginFieldDecoration(),
            controller: emailController,
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 5.0, bottom: 2.0),
            child: Text('Senha', style: GoogleFonts.rubik(fontSize: 14)),
          ),
          TextField(
            decoration: loginFieldDecoration(),
            controller: passwordController,
            obscureText: true,
          ),
          if (invalidLogin) invalidCredentialsLabel(),
          SizedBox(
            height: 30,
          ),
          loginButton(context),
          SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }

  Padding invalidCredentialsLabel() {
    return Padding(
      padding: const EdgeInsets.only(right: 8.0, top: 5.0),
      child: Text("Credenciais incorretas",
          textAlign: TextAlign.end,
          style: GoogleFonts.rubik(fontSize: 12, color: Colors.red)),
    );
  }

  Container loginButton(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      child: SizedBox(
        height: 48,
        child: TextButton(
          onPressed: () => loginBloc.add(TryLogin(
              credentials: LoginCredentials(
                  email: emailController.text,
                  password: passwordController.text))),
          child: Text(
            'ENTRAR',
            style: Theme.of(context).textTheme.button,
          ),
          style: TextButton.styleFrom(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
            backgroundColor: Theme.of(context).primaryColor,
          ),
        ),
      ),
    );
  }

  InputDecoration loginFieldDecoration() {
    return txtFieldDecoration(
        suffixIcon: invalidLogin
            ? Icon(
                Icons.cancel,
                color: Colors.red,
              )
            : null,
        borderSide:
            invalidLogin ? BorderSide(color: Colors.red, width: 2) : null);
  }

  OutlineInputBorder txtFieldInputBorder() {
    return OutlineInputBorder(
      borderSide: invalidLogin
          ? BorderSide(color: Colors.red, width: 2)
          : BorderSide.none,
      borderRadius: BorderRadius.circular(8.0),
    );
  }
}
