import 'package:flutter/material.dart';

class CustomAppBarShape extends CustomPainter {
  const CustomAppBarShape();

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint();

    Path path = Path();
    Rect pathGradientRect = Rect.fromCircle(
      center:  Offset(size.width / 4, 0),
      radius: size.width / 1.4,
    );

    Gradient gradient =  LinearGradient(
      begin: Alignment.bottomLeft,
      end: Alignment.topRight,
      colors: <Color>[
        Color(0xffe08db8),
        Color(0xffb2217a),
        Color(0xffb42391),
        Color(0xffae269b),
      ],
      stops: [
        0.2,
        0.5,
        0.8,
        1.0,
      ],
    );

    path.lineTo(-size.width / 1.7, 0);
    path.quadraticBezierTo(
        size.width / 2, size.height * 2, size.width + size.width / 1.7, 0);

    paint.color = Colors.deepOrange;
    paint.shader = gradient.createShader(pathGradientRect);
    paint.strokeWidth = 40;
    path.close();

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
