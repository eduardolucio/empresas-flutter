import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'custom_toolbar_shape.dart';

class CustomAppBar extends SliverPersistentHeaderDelegate {
  final double height;

  const CustomAppBar({required this.height});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      color: Colors.transparent,
      child: Stack(
        fit: StackFit.loose,
        children: <Widget>[
          Container(
            color: Colors.transparent,
            width: MediaQuery.of(context).size.width,
            height: height,
            child: CustomPaint(
              painter: CustomAppBarShape(),
            ),
          ),
          Container(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 20,),
                Image.asset('assets/logo.png'),
                SizedBox(height: 10,),
                if(shrinkOffset == 0) Text('Seja bem vindo ao empresas!', style: GoogleFonts.rubik(color: Colors.white, fontSize: 20),),
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  double get maxExtent => 230;

  @override
  double get minExtent => 100;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}
