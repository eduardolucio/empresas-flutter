import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:empresas_flutter/core/bloc/app_bloc.dart';
import 'package:empresas_flutter/core/errors/errors.dart';
import 'package:empresas_flutter/features/login/domain/entities/login_credentials.dart';
import 'package:empresas_flutter/features/login/domain/usecases/login.dart';
import 'package:equatable/equatable.dart';

part 'login_event.dart';
part 'login_state.dart';

const loginInvalidMessage = "Invalid login credentials";
const loginFailureMessage = "Something went wrong when trying to login";

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc({required this.appBloc, required this.login}) : super(Empty());

  final AppBloc appBloc;
  final Login login;

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is TryLogin) {
      final credentials = event.credentials;

      yield Loading();
      final either = await login(credentials);
      
      yield* either.fold(
        (l) async* {
          final String message = (l is LoginInvalidFailure) ? loginInvalidMessage : loginFailureMessage;
          yield LoginFailed(message: message);
        },
        (loggedUser) async*{
          appBloc.add(LogInEvent(loggedUser));
          yield LoginSuccess();
        }
      );

    }
  }
}
