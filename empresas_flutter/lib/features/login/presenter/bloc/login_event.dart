part of 'login_bloc.dart';

abstract class LoginEvent extends Equatable{}

class TryLogin extends LoginEvent{
  final LoginCredentials credentials;

  TryLogin({required this.credentials});

  @override
  List<Object?> get props => [];
}