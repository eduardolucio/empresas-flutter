part of 'login_bloc.dart';

abstract class LoginState extends Equatable {}

class Empty extends LoginState {
  @override
  List<Object?> get props => [];
}

class Loading extends LoginState {
  @override
  List<Object?> get props => [];
}

class LoginSuccess extends LoginState {
  @override
  List<Object?> get props => [];
}

class LoginFailed extends LoginState {
  final String message;

  LoginFailed({required this.message});

  @override
  List<Object?> get props => [message];
}