import 'package:empresas_flutter/features/login/domain/entities/logged_user.dart';

class LoggedUserModel extends LoggedUser {
  final DateTime expiry;

  LoggedUserModel(
      {required this.expiry,
      required String accessToken,
      required String client,
      required String uid})
      : super(accessToken: accessToken, client: client, uid: uid);

  factory LoggedUserModel.fromJson(Map<String, dynamic> jsonMap) {
    return LoggedUserModel(
      expiry: DateTime.parse(jsonMap['expiry']),
      accessToken: jsonMap['accessToken'],
      client: jsonMap['client'],
      uid: jsonMap['uid'],
    );
  }

  Map<String, dynamic> toJson() => {
    'expiry': expiry.toIso8601String(),
    'accessToken': accessToken,
    'client': client,
    'uid': uid
  };

  bool get isExpired => expiry.isBefore(DateTime.now());

  @override
  List<Object?> get props => super.props + [expiry];
}
