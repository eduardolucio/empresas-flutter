import 'package:empresas_flutter/core/errors/exceptions.dart';
import 'package:empresas_flutter/features/login/domain/entities/login_credentials.dart';
import 'package:empresas_flutter/features/login/domain/entities/logged_user.dart';
import 'package:empresas_flutter/core/errors/errors.dart';
import 'package:empresas_flutter/core/adapters/either_adapter.dart';
import 'package:empresas_flutter/features/login/domain/repositories/login_repository.dart';
import 'package:empresas_flutter/features/login/infra/datasources/login_caching_data_source.dart';
import 'package:empresas_flutter/features/login/infra/datasources/login_data_source.dart';

class LoginRepositoryImpl implements LoginRepository{
  final LoginDataSource loginDataSource;
  final LoginCachingDataSource cachingDataSource;

  LoginRepositoryImpl(this.loginDataSource, this.cachingDataSource);

  @override
  Future<EitherAdapter<Failure, LoggedUser>> login(LoginCredentials credentials) async{
    try {
      final loggedUser = await loginDataSource.login(credentials);
      cachingDataSource.cacheLoggedUser(loggedUser);
      return RightAdapter(loggedUser);
    } on InvalidLoginException {
      return LeftAdapter(LoginInvalidFailure());
    } catch(e) {
      return LeftAdapter(LoginFailure());
    }
  }

  @override
  Future<EitherAdapter<Failure, LoggedUser>> getLoggedUser() async{
    try {
      final userCached = await cachingDataSource.getUserCached();
      if(userCached.isExpired){
        return LeftAdapter(LoginExpiredFailure());
      }
      return RightAdapter(await cachingDataSource.getUserCached());
    } catch(e){
      return LeftAdapter(LoginCacheFailure());
    }
  }

}