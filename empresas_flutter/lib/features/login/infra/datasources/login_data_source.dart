import 'package:empresas_flutter/features/login/domain/entities/login_credentials.dart';
import 'package:empresas_flutter/features/login/infra/models/logged_user_model.dart';

abstract class LoginDataSource {
  Future<LoggedUserModel> login(LoginCredentials credentials);
}