import 'package:empresas_flutter/features/login/infra/models/logged_user_model.dart';

abstract class LoginCachingDataSource {
  Future<LoggedUserModel> getUserCached();
  Future<void> cacheLoggedUser(LoggedUserModel userModel);
}