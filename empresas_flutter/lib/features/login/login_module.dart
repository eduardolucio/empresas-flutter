import 'package:empresas_flutter/features/login/domain/usecases/login.dart';
import 'package:empresas_flutter/features/login/presenter/bloc/login_bloc.dart';
import 'package:empresas_flutter/features/login/presenter/pages/login_page.dart';
import 'package:flutter_modular/flutter_modular.dart';

class LoginModule extends Module {
  @override
  List<Bind<Object>> get binds => [
    Bind.lazySingleton((i) => LoginImpl(i())),
    Bind.factory((i) => LoginBloc(appBloc: i(), login: i()))
  ];

  @override
  List<ModularRoute> get routes => [
    ChildRoute('/', child: (_, args) => LoginPage())
  ];
}