import 'package:empresas_flutter/core/constants.dart';
import 'package:empresas_flutter/core/errors/exceptions.dart';
import 'package:empresas_flutter/core/utils/custom_dio.dart';
import 'package:empresas_flutter/features/login/domain/entities/login_credentials.dart';
import 'package:empresas_flutter/features/login/infra/datasources/login_data_source.dart';
import 'package:empresas_flutter/features/login/infra/models/logged_user_model.dart';

class LoginDataSourceImpl implements LoginDataSource {
  final CustomDio customDio;

  LoginDataSourceImpl({required this.customDio});

  get client => customDio.client;

  @override
  Future<LoggedUserModel> login(LoginCredentials credentials) async {
    final response = await client.post("$baseApiUrl/users/auth/sign_in",
        data: {'email': credentials.email, 'password': credentials.password});

    if (response.statusCode == 200) {
      return _extractUserModel(response.headers);
    } else if (response.statusCode == 401) {
      throw InvalidLoginException();
    } else {
      throw LoginErrorException();
    }
  }

  LoggedUserModel _extractUserModel(dynamic headers) {
    final accessToken = headers['access-token'];
    final uid = headers['uid'];
    final clientHeader = headers['client'];
    final expiry = headers['expiry'];

    if (accessToken == null ||
        uid == null ||
        clientHeader == null ||
        expiry == null) {
      throw LoginErrorException();
    } else {
      final expiryDate = DateTime.fromMillisecondsSinceEpoch(
          Duration(seconds: int.parse(expiry[0])).inMilliseconds);

      return LoggedUserModel(
          accessToken: accessToken[0],
          uid: uid[0],
          client: clientHeader[0],
          expiry: expiryDate);
    }
  }
}
