import 'dart:convert';

import 'package:empresas_flutter/core/errors/exceptions.dart';
import 'package:empresas_flutter/features/login/infra/datasources/login_caching_data_source.dart';
import 'package:empresas_flutter/features/login/infra/models/logged_user_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

const cacheLoggedUserString = "CACHE_LOGGED_USER";

class LoginCachingDataSourceImpl implements LoginCachingDataSource {
  final SharedPreferences sharedPreferences;

  LoginCachingDataSourceImpl(this.sharedPreferences);

  @override
  Future<void> cacheLoggedUser(LoggedUserModel userModel) async {
    sharedPreferences.setString(
        cacheLoggedUserString, json.encode(userModel.toJson()));
  }

  @override
  Future<LoggedUserModel> getUserCached() async {
    final jsonString = sharedPreferences.getString(cacheLoggedUserString);

    if (jsonString == null) {
      throw NoCachedDataException();
    } else {
      return LoggedUserModel.fromJson(json.decode(jsonString));
    }
  }
}
