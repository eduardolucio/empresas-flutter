import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

InputDecoration txtFieldDecoration({BorderSide? borderSide, Widget? suffixIcon, Widget? preffixIcon, String? hintText}) {
    return InputDecoration(
      hintText: hintText,
      hintStyle: GoogleFonts.rubik(fontWeight: FontWeight.w300,fontSize: 18, color: Color(0xff666666)),
      prefixIcon: preffixIcon,
      suffixIcon: suffixIcon,
      filled: true,
      fillColor: Color(0xffF5F5F5),
      enabledBorder: txtFieldInputBorder(borderSide: borderSide),
      focusedBorder: txtFieldInputBorder(borderSide: borderSide),
      
    );
  }


OutlineInputBorder txtFieldInputBorder({BorderSide? borderSide}) {
    return OutlineInputBorder(
      borderSide: borderSide ?? BorderSide.none,
      borderRadius: BorderRadius.circular(8.0),
    );
  }
  