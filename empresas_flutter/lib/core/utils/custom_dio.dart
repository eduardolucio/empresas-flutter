import 'package:dio/dio.dart';
import 'package:empresas_flutter/core/bloc/app_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class CustomDio{
  final Dio client;
  final AppBloc appBloc;

  CustomDio({required this.client, required this.appBloc}){
    this.client.interceptors.add(
      InterceptorsWrapper(
        onRequest: (options, handler){
          RequestOptions requestOptions = options;
          
          final state = appBloc.state;
          if(state is Authenticated){
            requestOptions = options.copyWith(headers: {
              'access-token': state.loggedUser.accessToken,
              'uid': state.loggedUser.uid,
              'client': state.loggedUser.client 
            });
          }

          handler.next(requestOptions);
        },
        onError: (DioError e, handler){
          if(e.type == DioErrorType.response){
            if(e.response?.statusCode == 401 && Modular.to.path != '/login/'){
              Modular.to.pushNamedAndRemoveUntil('/login', ModalRoute.withName('/'));
            }
          }

          handler.next(e);
        }
      )
    );
  }
}