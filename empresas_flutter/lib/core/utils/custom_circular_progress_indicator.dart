import 'package:flutter/material.dart';

class CustomCircularProgressIndicator extends StatefulWidget {
  const CustomCircularProgressIndicator({Key? key}) : super(key: key);

  @override
  _CustomCircularProgressIndicatorState createState() =>
      _CustomCircularProgressIndicatorState();
}

class _CustomCircularProgressIndicatorState
    extends State<CustomCircularProgressIndicator>
    with SingleTickerProviderStateMixin {
  late AnimationController animationController;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1),
      lowerBound: 0,
      upperBound: 6.28,
    );

    animationController.repeat();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: animationController,
        child: Container(
          width: 72,
          height: 72,
          child: CustomPaint(
            painter: PainterIndicator(),
          ),
        ),
        builder: (context, _widget) {
          return Transform.rotate(
              angle: animationController.value, child: _widget);
        });
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }
}

class PainterIndicator extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final center = Offset(size.width / 2, size.height / 2);

    canvas.drawArc(
      Rect.fromCenter(center: center, width: 72, height: 72),
      -1.5,
      4.5,
      false,
      Paint()
        ..style = PaintingStyle.stroke
        ..strokeCap = StrokeCap.round
        ..color = Color(0xffFBDBE7)
        ..strokeWidth = 3
        ..blendMode = BlendMode.srcIn,
    );

    canvas.drawArc(
      Rect.fromCenter(center: center, width: 47, height: 47),
      1.5,
      4.5,
      false,
      Paint()
        ..style = PaintingStyle.stroke
        ..strokeCap = StrokeCap.round
        ..color = Color(0xffFBDBE7)
        ..strokeWidth = 3
        ..blendMode = BlendMode.srcIn,
    );

  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}
