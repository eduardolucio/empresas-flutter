import 'package:equatable/equatable.dart';
import 'package:fpdart/fpdart.dart' as fpdart;

abstract class EitherAdapter<Left,Right> {
  dynamic fold(dynamic Function(Left l) leftF, dynamic Function(Right r) rightF);
}

abstract class EitherAdapterFpDart<L,R> extends Equatable implements EitherAdapter<L,R>  {
  final fpdart.Either<L,R> either;

  EitherAdapterFpDart(this.either);

  @override
  fold(Function(L l) leftF, Function(R r) rightF) {
    return either.fold(leftF, rightF);
  }
}

class RightAdapter<L,R> extends EitherAdapterFpDart<L,R>{
  final R r;
  RightAdapter(this.r) : super(fpdart.Right(r));

  @override
  List<Object?> get props => [r];
}

class LeftAdapter<L,R> extends EitherAdapterFpDart<L,R>{
  final L l;
  LeftAdapter(this.l) : super(fpdart.Left(l));

  @override
  List<Object?> get props => [l];
}