import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:empresas_flutter/features/login/domain/entities/logged_user.dart';
import 'package:equatable/equatable.dart';

part 'app_event.dart';
part 'app_state.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  AppBloc() : super(Unauthenticated());

  @override
  Stream<AppState> mapEventToState(
    AppEvent event,
  ) async* {
    if(event is LogInEvent){
      yield Authenticated(event.loggedUser);
    }else if(event is ExpiredLoginEvent){
      yield Unauthenticated();
    }
  }
}
