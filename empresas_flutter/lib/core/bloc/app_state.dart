part of 'app_bloc.dart';

abstract class AppState extends Equatable {}

class Unauthenticated extends AppState {
  List<Object?> get props => [];
}

class Authenticated extends AppState {
  final LoggedUser loggedUser;

  Authenticated(this.loggedUser);

  @override
  List<Object?> get props => [loggedUser];
}
