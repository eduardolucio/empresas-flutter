part of 'app_bloc.dart';

abstract class AppEvent extends Equatable{}

class LogInEvent extends AppEvent{
  final LoggedUser loggedUser;

  LogInEvent(this.loggedUser);

  @override
  List<Object?> get props => [loggedUser];
}

class ExpiredLoginEvent extends AppEvent{
  @override
  List<Object?> get props => [];
}
