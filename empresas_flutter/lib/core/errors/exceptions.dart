class NoCachedDataException implements Exception {}

class InvalidLoginException implements Exception {}

class LoginErrorException implements Exception {}

class ServerException implements Exception {}