import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable{
  @override
  List<Object?> get props => [];
}

class LoginFailure extends Failure {}

class LoginInvalidFailure extends Failure {}

class LoginCacheFailure extends Failure{}

class LoginEmailFailure extends Failure{}

class LoginExpiredFailure extends Failure{}

class ServerFailure extends Failure {}