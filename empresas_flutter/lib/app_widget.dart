import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class AppWidget extends StatelessWidget {
  const AppWidget({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Color(0xffE01E69),
        backgroundColor: Colors.white,
        primaryColorDark: Color(0xff666666),
        primaryColorLight: Color(0xffF5F5F5),
        textTheme: TextTheme(
          button: TextStyle(
            fontSize: 16,
            color: Colors.white
          )
        )
      ),
      initialRoute: '/',
    ).modular();
  }
}