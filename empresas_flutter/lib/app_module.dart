import 'package:dio/dio.dart';
import 'package:empresas_flutter/core/bloc/app_bloc.dart';
import 'package:empresas_flutter/core/utils/custom_dio.dart';
import 'package:empresas_flutter/features/companies/presenter/companies_module.dart';
import 'package:empresas_flutter/features/login/domain/usecases/get_logged_user.dart';
import 'package:empresas_flutter/features/login/external/datasources/login_caching_data_source_impl.dart';
import 'package:empresas_flutter/features/login/external/datasources/login_data_source_impl.dart';
import 'package:empresas_flutter/features/login/infra/repositories/login_repository_impl.dart';
import 'package:empresas_flutter/features/login/login_module.dart';
import 'package:empresas_flutter/features/splash_screen/bloc/splash_screen_bloc.dart';
import 'package:empresas_flutter/features/splash_screen/splash_screen_page.dart';
import 'package:empresas_flutter/main.dart';
import 'package:flutter_modular/flutter_modular.dart';

class AppModule extends Module {
  @override
  List<Bind<Object>> get binds => [
    Bind.lazySingleton((i) => AppBloc()),
    Bind.lazySingleton((i) => Dio()),
    Bind.lazySingleton((i) => CustomDio(client: i(), appBloc: i())),
    Bind.lazySingleton((i) => LoginDataSourceImpl(customDio: i())),
    Bind((i) => sharedPreferences),
    Bind.lazySingleton((i) => LoginCachingDataSourceImpl(i())),
    Bind.lazySingleton((i) => LoginRepositoryImpl(i(), i())),
    Bind.lazySingleton((i) => GetLoggedUserImpl(i())),
    Bind.factory((i) => SplashScreenBloc(getLoggedUser: i(), appBloc: i()))
  ];

  @override
  List<ModularRoute> get routes => [
    ChildRoute('/', child: (_, args) => SplashScreenPage()),
    ModuleRoute('/login', module: LoginModule()),
    ModuleRoute('/companies', module: CompaniesModule())
  ];
}